/* eslint-env node */

module.exports = function(environment) {
  var ENV = {
    modulePrefix: 'apex-ui',
    environment: environment,
    rootURL: '/',
    locationType: 'auto',
    EmberENV: {
      FEATURES: {
        // Here you can enable experimental features on an ember canary build
        // e.g. 'with-controller': true
      },
      EXTEND_PROTOTYPES: {
        // Prevent Ember Data from overriding Date.parse.
        Date: false
      }
    },

    APP: {
      // Here you can pass flags/options to your application instance
      // when it is created
    },

    apiURL: '',
    defaultPollDelay: 2000,
    assessments : [
      { assessmentId: 'hand_up', title: 'Raise Hand' },
      { assessmentId: 'hand_forward', title: 'Move Hand Forward' },
      { assessmentId: 'hand_right', title: 'Move Hand Right' },
      { assessmentId: 'hand_left', title: 'Move Hand Left' },
      { assessmentId: 'joystick_1_forward', title: 'Move L_Joystick Forward' },
      { assessmentId: 'joystick_1_right', title: 'Move L_Joystick Right' },
      { assessmentId: 'joystick_1_left', title: 'Move L_Joystick Left' },
      { assessmentId: 'joystick_2_forward', title: 'Move R_Joystick Forward' },
      { assessmentId: 'joystick_2_right', title: 'Move R_Joystick Right' },
      { assessmentId: 'joystick_2_left', title: 'Move R_Joystick Left' },
      { assessmentId: 'ergo_right', title: 'Move Ergo Robot Right' },
      { assessmentId: 'ergo_left', title: 'Move Ergo Robot Left' },
      { assessmentId: 'ball_right', title: 'Move Ball Right' },
      { assessmentId: 'ball_left', title: 'Move Ball Left' },
      { assessmentId: 'light', title: 'Change Light Color' },
      { assessmentId: 'sound', title: 'Make Sound' }
    ],
    chartColors: [ '#7cb5ec', '#f45b5b', '#90ed7d', '#f7a35c', '#8085e9', '#e4d354', '#2b908f', '#434348' ]
  };

  if (environment === 'development') {
    // ENV.APP.LOG_RESOLVER = true;
    // ENV.APP.LOG_ACTIVE_GENERATION = true;
    // ENV.APP.LOG_TRANSITIONS = true;
    // ENV.APP.LOG_TRANSITIONS_INTERNAL = true;
    // ENV.APP.LOG_VIEW_LOOKUPS = true;
    ENV.apiURL = 'http://mingew.local:5000';
  }

  if (environment === 'test') {
    // Testem prefers this...
    ENV.locationType = 'none';

    // keep test console output quieter
    ENV.APP.LOG_ACTIVE_GENERATION = false;
    ENV.APP.LOG_VIEW_LOOKUPS = false;

    ENV.APP.rootElement = '#ember-testing';
  }

  if (environment === 'production') {

  }

  return ENV;
};
