import Ember from 'ember';
import config from '../config/environment';

const { Route, getWithDefault, run, $ } = Ember;

export default Route.extend({

  pollTimer: null,

  model() {
    return $.getJSON(`${config.apiURL}/api/interests`).then((data) => {
      const pollDelay = getWithDefault(data, 'pollDelay', config.defaultPollDelay);
      const timer = run.later(null, () => { this.refresh(); }, pollDelay);
      this.set('pollTimer', timer);
      if (data.roundPointInterval === true) {
        data.pointInterval = Math.round(data.pointInterval);
      }
      return data;
    });
  },

  deactivate() {
    this._super(...arguments);
    run.cancel(this.get('pollTimer'));
  },

  actions: {
    updateFocus(interestId) {
      return $.post(`${config.apiURL}/api/focus`, { interestId });
    },
    cancelFocus() {
      return $.post(`${config.apiURL}/api/focus`, { interestId: null });
    },
    timeTravel(pointX) {
      return $.post(`${config.apiURL}/api/time-travel`, { point: pointX });
    },
    updateAssessment(assessmentId) {
      return $.post(`${config.apiURL}/api/assessment`, { assessmentId });
    }
  }
});
