import Ember from 'ember';
import config from '../config/environment';

const { Component, computed, isBlank } = Ember;

export default Component.extend({

  selectedPoint: null,

  askConfirmation: false,

  isSelectedPointValid: computed('selectedPoint', function() {
    const pointX = this.get('selectedPoint');

    return !(isBlank(pointX) || Number.isNaN(pointX) || pointX < 0);
  }),

  resetDisabled: computed('isSelectedPointValid', 'isBusy', function() {
    return !(this.get('isSelectedPointValid') && !this.get('isBusy'));
  }),

  chartOptions: computed(function() {
    return {
      colors: config.chartColors,
      chart: {
        style: {
          fontFamily: 'Roboto',
          fontSize: '20px'
        },
        zoomType: 'x'
      },
      title: null,
      legend: {
        itemStyle: { fontSize: '18px' }
      },
      xAxis: {
        allowDecimals: false,
        labels: {
          formatter: function() {
            return this.value;
          }
        }
      },
      yAxis: {
        max: 1,
        title: null
      },
      tooltip: {
        shared: true,
        crosshairs: true
      },
      plotOptions: {
        line: {
          pointStart: 0,
          pointInterval: this.get('pointInterval'),
          lineWidth: 3,
          marker: {
            enabled: false,
            symbol: 'circle',
            radius: 2,
            states: {
              hover: { enabled: true }
            }
          }
        },
        series: {
          cursor: 'pointer',
          point: {
            events: {
              click: (e) => this.send('selectPoint', e.point.x)
            }
          },
          marker: { lineWidth: 1 }
        }
      },
      credits: { enabled: false }
    };
  }),

  chartData: computed.map('dataset', function(interest) {
    const { data, interestId } = interest;
    const name = this.get('interests').findBy('interestId', interestId).title;

    return { name, data };
  }),

  actions: {
    selectPoint(pointX) {
      this.set('selectedPoint', pointX);
    },
    requestTimeTravel() {
      this.set('askConfirmation', true);
    },
    cancelTimeTravel() {
      this.set('askConfirmation', false);
    },
    confirmTimeTravel() {
      if (this.get('isBusy') === false) {
        this.set('askConfirmation', false);
        this.set('isBusy', true);
        this.sendAction('timeTravelAction', this.get('selectedPoint'));
      }
    }
  }
});
