import Ember from 'ember';
import config from '../config/environment';

const { Component, computed } = Ember;

export default Component.extend({

  /**
   * @property chartOptions
   * @type {Object}
   */
  chartOptions: computed(function() {
    return {
      chart: {
        type: 'solidgauge'
      },
      title: null,
      pane: {
        center: [ '50%', '85%' ],
        size: '160%',
        startAngle: -90,
        endAngle: 90,
        background: {
          innerRadius: '60%',
          outerRadius: '100%',
          shape: 'arc'
        }
      },
      tooltip: { enabled: false },
      yAxis: {
        labels: { y: 16 },
        lineWidth: 0,
        max: 100,
        min: 0,
        minorTickInterval: null,
        stops: false,
        maxColor: config.chartColors[this.get('position')],
        tickAmount: 2,
        tickLength: 0
      },
      plotOptions: {
        solidgauge: {
          dataLabels: {
            borderWidth: 0,
            format: '<div class="gauge-legend"><span class="gauge-legend-value">{y:.0f}</span><br><span class="gauge-legend-unit">%</span></div>',
            useHTML: true,
            y: 90
          }
        }
      }
    };
  }),

  /**
   * Focused state
   *
   * @property isFocused
   * @type {Ember.ComputedProperty}
   */
  isFocused: computed('interest.interestId', 'focusedInterest', function() {
    const focusedInterest = this.get('focusedInterest');
    const interestId = this.get('interest.interestId');

    return focusedInterest === interestId;
  }),

  /**
   * @property chartData
   * @type {Ember.ComputedProperty}
   */
  chartData: computed('interest.title', 'interest.value', function() {
    return [{
      name: this.get('interest.title'),
      data: [ 100 * this.get('interest.value') ],
      tooltip: {
        valueSuffix: ' %'
      }
    }];
  }),

  actions: {
    toggleFocus() {
      if (this.get('isFocused') === false) {
        this.focusHandler(this.get('interest.interestId'));
      } else {
        this.focusHandler(null);
      }
    }
  }
});
