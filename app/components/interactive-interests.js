import Ember from 'ember';

const { Component, computed } = Ember;

export default Component.extend({

  chartData: computed.map('dataset', function(interest) {
    const { data, interestId } = interest;
    const name = this.get('interests').findBy('interestId', interestId).title;

    return { name, data };
  }),

  actions: {
    focusInterest(interestId) {
      this.set('isBusy', true);
      this.set('focusedInterest', interestId);
      this.sendAction('updateFocusAction', interestId);
    }
  }
});
