import Ember from 'ember';
import EmberHighChartsComponent from 'ember-highcharts/components/high-charts';

const { observer } = Ember;

export default EmberHighChartsComponent.extend({

  contentDidChange: observer('content.@each.isLoaded', function() {
    const chart = this.get('chart');
    const pointY = this.get('content')[0].data;
    const point = chart.series[0].points[0];
    point.update(pointY);
  })
});
