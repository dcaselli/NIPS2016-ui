import Ember from 'ember';
import config from '../config/environment';

const { Component, computed, isBlank } = Ember;

export default Component.extend({

  selectedValue: '',

  assessments: config.assessments,

  isSelectedValueValid: computed('selectedValue', function() {
    return !isBlank(this.get('selectedValue'));
  }),

  demoDisabled: computed('isSelectedValueValid', 'isBusy', function() {
    return !(this.get('isSelectedValueValid') && !this.get('isBusy'));
  }),

  actions: {
    selectChanged() {
      const selectedValue = this.$('select').val();
      this.set('selectedValue', selectedValue);
    },
    showDemo() {
      if (!isBlank(this.get('demoDisabled'))) {
        this.set('isBusy', true);
        this.sendAction('showDemoAction', this.get('selectedValue'));
      }
    }
  }
});
